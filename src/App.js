import React from 'react';
import { Route, Routes } from 'react-router-dom'
// import { Outlet } from 'react-router-dom';
import './App.css';
import './js/App.js';
// import Navbar from './components/Navbar'
// import Sidebar from './components/Sidebar'
import AllNavbar from './components/AllNavbar'
import Login from './components/Login'
import ManageUsers from './components/ManageUsers'
import ManageIcons from './components/ManageIcons'
import Logout from './components/Logout';
import ManageAdmin from './components/ManageAdmin';

const App = () => {
  return (
    <>
      <Routes >
        <Route path='/' element={<Login />} />
      </Routes>

      <div className='body-wrapper-main'>
        <div className='bodywrapper__inner'>
          <div className='body-wrapper'>
            <Routes >


              <Route element={<AllNavbar/>}>
                <Route path='/manageusers' element={<ManageUsers />} />
              </Route>

              <Route element={<AllNavbar/>}>
                <Route path='/manageicons' element={<ManageIcons />} />
              </Route>

              <Route element={<AllNavbar/>}>
                <Route path='/manageadmins' element={<ManageAdmin />} />
              </Route>

              <Route element={<AllNavbar/>}>
                <Route path='/logout' element={<Logout />} />
              </Route>

            </Routes>

          </div>
        </div>
      </div>
    </>
  );
}

export default App;
