import React, {useEffect} from 'react'
import { useNavigate } from 'react-router-dom'

const Logout = () => {

    const navigate = useNavigate();

    const MakeLogout = async() => {
        const response = await fetch("http://54.95.193.11:4001/logout",{
            method: "POST",
            credentials: 'include',
            headers: {
                Accept: "application/json",
                "Content-Type" : "application/json"
            },
            credentials : "include"
        });

        const data = await response.json();

        if(data.status){
            navigate("/");
        }
    }

    useEffect(()=>{
        MakeLogout()
    },[])

    return (
        <></>
    )
}

export default Logout