import React from "react";
import {NavLink} from 'react-router-dom';

const Sidebar = () => {
    return (
        <>
            <div className="sidebar sidebar capsule--rounded bg_img overlay--indigo overlay--opacity-8" data-background="">
                <button className="res-sidebar-close-btn"><i className="las la-times"></i></button>
                <div className="sidebar__inner">
                    <div className="sidebar__logo">
                        <NavLink to="/" className="sidebar__main-logo">
                        <img src="/logo.png" className=''></img>
                        <span className="logo-text">Huge icon</span></NavLink>
                        <NavLink to="/" className="sidebar__logo-shape">
                        <img src="" alt="" /></NavLink>
                        <button type="button" className="navbar__expand"></button>
                    </div>
                    <div className="sidebar__menu-wrapper" id="sidebar__menuWrapper">
                        <ul className="sidebar__menu">
                            {/* <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/manageicons"> 
                                    <i className="menu-icon las la-home"></i>
                                    <span className="menu-title">Dashboard</span>
                                </NavLink>
                            </li> */}
                            <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/manageicons"> 
                                    <i className="menu-icon las la-home"></i>
                                    <span className="menu-title">Manage Icons</span>
                                </NavLink>
                            </li>
                            <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/manageusers">
                                    <i className="menu-icon las la-users"></i>
                                    <span className="menu-title">Manage Users</span>
                                </NavLink>
                            </li>
                            {/* <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/manageadmins">
                                    <i className="menu-icon las la-users"></i>
                                    <span className="menu-title">Manage Admins</span>
                                </NavLink>
                            </li> */}
                            <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/logout">
                                <i className="menu-icon las la-sign-out-alt"></i>
                                    <span className="menu-title">Logout</span>
                                </NavLink>
                            </li>
                            {/* <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/login">
                                    <i className="menu-icon las la-home"></i>
                                    <span className="menu-title">Login</span>
                                </NavLink>
                            </li>
                            <li className="sidebar-menu-item">
                                <NavLink className="nav-link" to="/logout">
                                    <i className="menu-icon las la-home"></i>
                                    <span className="menu-title">Logout</span>
                                </NavLink>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Sidebar;