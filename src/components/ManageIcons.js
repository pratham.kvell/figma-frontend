import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import $ from "jquery";
import Swal from 'sweetalert2';
import axios from 'axios';
import bootstrap from 'bootstrap/dist/js/bootstrap';

const ManageIcons = () => {

    const [iconList, setIconList] = useState([]);
    const [search, setsearch] = useState("");
    const [fileurl, setFileUrl] = useState("");
    const [baseurl, setBaseUrl] = useState("");
    const [icon, setIcon] = useState({
        name : "",
        uploadType: "1",
        image: "",
        type: "free",
        style: "stroke",
        tags: ""
    });

    const [editImage, setEditImage] = useState("");

    const navigate = useNavigate();

    const getIcons = async() => {

        $(".loader").show();
        $(".icon-table").hide();

        const response = await fetch('http://54.95.193.11:4001/icons',{
            method: "GET",
            credentials: 'include',
        });
        const data = await response.json();

        if(data.error === "Unauthorized"){
            navigate("/");
        }

        if(data.status){
            setIconList(data.icons);
            setBaseUrl(data.baseurl);
            $(".loader").hide();
            $(".icon-table").show();
        }
    }

    const deleteIcon = async(icon_id) => {
        const response = await fetch(`http://54.95.193.11:4001/delete-icons/${icon_id}`,{
            method: "POST",
            credentials: 'include',
        });
        const data = await response.json();

        if(data.status){
            getIcons();
        }
    }

    const EditIcon = async(icon_id) => {
        const response = await fetch(`http://54.95.193.11:4001/edit-icons/${icon_id}`,{
            method: "POST",
            credentials: 'include',
        });
        const data = await response.json();

        // console.log(data.icon[0]);return;

        if(data.status){
            if(data.icon.length === 1){
                $("#edit_name").val(data.icon[0].icon_name);
                $("#edit_style").val(data.icon[0].icon_style);
                $("#edit_type").val(data.icon[0].icon_type);
                $("#edit_tags").val(data.icon[0].icon_tags);
                $("#edit_iconid").val(data.icon[0].icon_id);

                var myModal = new bootstrap.Modal(document.getElementById('EditIcon'), {
                    keyboard: false
                });

                myModal.toggle();
            }
        }
    }

    const EditData = async(e) => {
        e.preventDefault();
        var formdata =  new FormData();

        var edit_name = $("#edit_name").val();
        var edit_style = $("#edit_style").val();
        var edit_type = $("#edit_type").val();
        var edit_tags = $("#edit_tags").val();
        var edit_iconid = $("#edit_iconid").val();
        var edit_image = null;

        if(editImage != ""){
            edit_image = editImage;
        }

        formdata.append('icon_name', edit_name);
        formdata.append('image', edit_image);
        formdata.append('icon_type', edit_type);
        formdata.append('icon_style', edit_style);
        formdata.append('icon_tags', edit_tags);
        formdata.append('icon_id', edit_iconid);

        const res = await axios.post("http://54.95.193.11:4001/update-icons",formdata, {
            withCredentials: true,
            headers: {
                "Content-Type" : "multipart/form-data"
            }
        })

        if(res.data.status){
            new Swal(
                "Done!",
                "Icon Updated!",
                "success",
            );
        }else{
            new Swal(
                "Oooops!",
                res.data.msg,
                "error",
            );
        }

        getIcons();

        $('#EditIcon').removeClass('show').addClass('hide');
        $('.modal-backdrop').removeClass('show').addClass('hide');
    }

    // let name;
    let searchValue ;

    const searchIcon = async(e) => {
        searchValue = e.target.value;
        if(searchValue !== ""){
            setsearch(searchValue);

            const response = await fetch("http://54.95.193.11:4001/search-icons",{
                method : "POST",
                credentials: 'include',
                headers: {
                    "Content-Type" : "application/json"
                },
                body: JSON.stringify({search_val : searchValue})
            })

            const data = await response.json();

            if(data.status){
                setIconList(data.icons);
            }
        }else{
            setsearch("");
            getIcons();
        }
    }

    let name , value;
    const handleInput = (e) => {
        name = e.target.name;
        value = e.target.value;
        setIcon({...icon, [name]:value});
    }

    const Postdata = async(e) => {
        e.preventDefault();
        var formdata =  new FormData();
        formdata.append('name', icon.name);
        formdata.append('uploadType', icon.uploadType);
        formdata.append('image', icon.image);
        formdata.append('type', icon.type);
        formdata.append('style', icon.style);
        formdata.append('tags', icon.tags);

        const res = await axios.post("http://54.95.193.11:4001/icons",formdata, {
            withCredentials: true,
            headers: {
                "Content-Type" : "multipart/form-data"
            }
        })

        if(res.data.status){
            new Swal(
                "Done!",
                "New Icon Created!",
                "success",
            );
        }else{
            new Swal(
                "Oooops!",
                res.data.msg,
                "error",
            );
        }

        $('#addNewIcon').removeClass('show').addClass('hide');
        $('.modal-backdrop').removeClass('show').addClass('hide');

        getIcons();
    }

    const updateUpload = () => {
        if ($("input[name='icon_upload']:checked").val() == 2) {
            $("#name").hide();
            $("#tags").hide();

            setIcon(prev => ({
                ...prev,
                name: "",
                tags: "",
                uploadType : "2"
            }))

        }else{
            $("#name").show();
            $("#tags").show();

            setIcon(prev => ({
                ...prev,
                name: "",
                tags: "",
                uploadType : "1"
            }))
        }
    }

    useEffect(() => {
       getIcons();
    }, []);

    return(
        <div>
            <div className='d-flex justify-content-end'>
                
                <div className='search-wrap'>
                    <input placeholder='Search Icon' className='form-control' id='searchicon' value={search} onChange={searchIcon}/>
                </div>
                <div className=' text-right'>
                    <a className='btn btn-theme' data-bs-toggle="modal" data-bs-target="#addNewIcon">Add new Icon</a>
                </div>
            </div>

            <div className='row my-4'>
                <div className='col-sm-12 d-flex justify-content-center'>
                    <div className="loader">
                        <span className="spinner-grow spinner-grow-sm mr-3" role="status" aria-hidden="true"></span>
                        <span className='ml-3'>Loading data. Please wait...</span>
                    </div>
                </div>
            </div>

            <div className='row'>
                <div className='col-lg-12'>
                    <div className='card b-radius--10'>
                        <div className='card-body p-0'>
                            <div className="table-responsive--md  table-responsive icon-table">
                                <table className="table table--light style--two">
                                    <thead>
                                        <tr>
                                            <th>Serial number</th>
                                            <th>Uploaded on</th>
                                            <th>Uploaded By</th>
                                            <th>Icon Name</th>
                                            {/* <th>Icon Image</th> */}
                                            {/* <th>Icon Style</th> */}
                                            <th>Icon Type</th> 
                                            <th>Last Edited</th> 
                                            <th>Icon Tags</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            iconList.map((val,i)=>{
                                                return(
                                                    <tr key={i}>
                                                        <td>{i+1}</td>
                                                        <td className='text-capitalize'>{val.addedon.substr(0,10)}</td>
                                                        <td className='text-capitalize'>{val.name}</td>
                                                        <td>{val.icon_name}</td>
                                                        {/* <td>{val.icon_image != "" ? <img id='icon_img' src={baseurl+val.icon_image} height="60px" width="60px"/> : ""}</td> */}
                                                        {/* <td>{val.icon_style}</td> */}
                                                        <td>{val.icon_type}</td>
                                                        <td>{val.updated_on.substr(0,10)}</td>
                                                        <td>{val.icon_tags}</td>
                                                        <td className='text-right'>
                                                            <a onClick={()=>{EditIcon(val.icon_id)}} className='icon-btn btn btn--primary mx-1'>Edit</a>
                                                            <a onClick={()=>{deleteIcon(val.icon_id)}} className='icon-btn btn btn--danger'>Delete</a>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="addNewIcon" tabIndex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Add New Icon</h5>
                                <button type="button" className="close btn " data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body">
                            <form encType='multipart/form-data'>
                                
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="icon_upload" id="icon_upload" value="1" onChange={updateUpload} defaultChecked/>
                                    <label className="form-check-label" htmlFor="icon_upload">
                                        Single Icon Upload
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="icon_upload" id="icon_upload2" value="2" onChange={updateUpload}/>
                                    <label className="form-check-label" htmlFor="icon_upload2">
                                        Multiple Icons Upload
                                    </label>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="text" className='form-control' required placeholder='Icon Name' name='name' id='name' value={icon.name} onChange={handleInput}/>
                                </div>
                                <div className='form-group my-3'>
                                    <label>Icon File</label>
                                    <input type="file" className='form-control' name='images' id='images' required onChange={(e)=>{
                                        setIcon(prev => ({
                                            ...prev,
                                            image: e.target.files[0]
                                        }))
                                        setFileUrl(URL.createObjectURL(e.target.files[0]))
                                    }}/>
                                </div>
                                <div className='form-group my-3'>
                                    <select className='form-select' name='style' onChange={handleInput} value={icon.style} id='style'>
                                        <option value="stroke" >Stroke</option>
                                        <option value="solid">Solid</option>
                                        <option value="doutone">Doutone</option>
                                        <option value="twocolors">Twocolors</option>
                                    </select>
                                </div>
                                <div className='form-group my-3'>
                                    <select className='form-select' name='type' onChange={handleInput} value={icon.type} id='type'>
                                        <option value="free" >Free</option>
                                        <option value="paid">Paid</option>
                                    </select>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="text" className='form-control' name='tags' onChange={handleInput} id='tags' value={icon.tags} required placeholder='Icon Tags'/>
                                </div>
                                <div className='form-group my-3'>
                                    <a type="submit" onClick={Postdata} className='form-control btn btn-primary'>Submit</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="EditIcon" tabIndex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Edit Icon</h5>
                                <button type="button" className="close btn" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body">
                            <form method='post'>
                                <div className='form-group my-3'>
                                    <input type="text" className='form-control' name='editname' id='edit_name' required placeholder='Icon Name'/>
                                </div>
                                <div className='form-group my-3'>
                                    <label>Icon File</label>
                                    <input type="file" className='form-control' name='images' id='images' onChange={(e)=>{
                                        setEditImage(e.target.files[0])
                                    }} required/>
                                </div>
                                <div className='form-group my-3'>
                                    <select className='form-select' name='style' id='edit_style' value="stroke">
                                        <option value="stroke" >Stroke</option>
                                        <option value="solid">Solid</option>
                                        <option value="doutone">Doutone</option>
                                        <option value="twocolors">Twocolors</option>
                                    </select>
                                </div>
                                <div className='form-group my-3'>
                                    <select className='form-select' name='type' id='edit_type' value="free">
                                        <option value="free">Free</option>
                                        <option value="paid">Paid</option>
                                    </select>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="text" className='form-control' name='tags' id='edit_tags' required placeholder='Icon Tags'/>
                                </div>
                                    <input type="text" name='iconid' id='edit_iconid' hidden/>
                                <div className='form-group my-3'>
                                    <a type="submit" onClick={EditData} className='form-control btn btn-primary'>Submit</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    )

}

export default ManageIcons;
