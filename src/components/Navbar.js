import React from 'react';
// import {NavLink} from 'react-router-dom';

const Navbar = () => {
    return(
        <>
        <nav className="navbar-wrapper">
            <form className="navbar-search" onSubmit="return false;">
                <button type="submit" className="navbar-search__btn">
                    <i className="las la-search"></i>
                </button>
                <input type="search" name="navbar-search__field" id="navbar-search__field" placeholder="Search..." />
                <button type="button" className="navbar-search__close"><i className="las la-times"></i></button>
                <div id="navbar_search_result_area">
                    <ul className="navbar_search_result"></ul>
                </div>
            </form>
            <div className="navbar__left">
                <button className="res-sidebar-open-btn"><i className="las la-bars"></i></button>
                <button type="button" className="fullscreen-btn">
                    <i className="fullscreen-open las la-compress" onClick="openFullscreen()"></i>
                    <i className="fullscreen-close las la-compress-arrows-alt" onClick="closeFullscreen()"></i>
                </button>
            </div>

            <div className="navbar__right">
                <ul className="navbar__action-list">
                    {/* <li className="dropdown">
                        <NavLink className="nav-link" to="/login">
                            <i className="menu-icon las la-home"></i>
                            <span className="menu-title">Login</span>
                        </NavLink>
                    </li> */}
                    {/* <li className="dropdown">
                        <NavLink className="nav-link" to="/logout">
                            <i className="menu-icon las la-home"></i>
                            <span className="menu-title">Logout</span>
                        </NavLink>
                    </li> */}
                </ul>
            </div>
        </nav>
        </>
    )
}

export default Navbar;