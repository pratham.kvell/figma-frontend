import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import $ from "jquery";
import bootstrap from 'bootstrap/dist/js/bootstrap';

const ManageAdmin = () => {

    const [adminsList, setAdmins] = useState([]);
    const [search, setsearch] = useState("");
    const [admin, setAdmin] = useState({
        name : "",
        email: "",
        password: ""
    })

    const navigate = useNavigate();

    const getAdmins = async() => {
        const response = await fetch("http://54.95.193.11:4001/admin-users",{
            method: "GET",
            credentials: "include"
        });

        const data = await response.json();

        if(data.error === "Unauthorized"){
            navigate("/");
        }
        if(data.status){
            setAdmins(data.users);
        }
    }

    const searchAdmin = async(e) => {
        value = e.target.value;
        if(value !== ""){
            setsearch(value);

            const response = await fetch("http://54.95.193.11:4001/search-admin",{
                method : "POST",
            credentials: 'include',
                headers: {
                    "Content-Type" : "application/json"
                },
                body: JSON.stringify({search_val : value})
            })

            const data = await response.json();

            if(data.status){
                setAdmins(data.users);
            }
        }else{
            setsearch("");
            getAdmins();
        }
    }

    let name , value;
    const handleInput = (e) => {
        name = e.target.name;
        value = e.target.value;
        setAdmin({...admin, [name]:value});
    }

    const EditUser = async(user_id) => {
        const response = await fetch(`http://54.95.193.11:4001/edit-users/${user_id}`,{
            method: "POST",
            credentials: 'include',
        });
        const data = await response.json();

        if(data.status){
            if(data.users.length === 1){
                var editname = data.users[0].name;
                var editemail = data.users[0].email;
                var edituser_id = data.users[0].user_id;

                $("#name").val(editname);
                $("#email").val(editemail);
                $("#userid").val(edituser_id);

                var myModal = new bootstrap.Modal(document.getElementById('EditIcon'), {
                    keyboard: false
                });

                myModal.toggle();
            }
        }
    }

    const deleteUser = async(user_id) => {
        const response = await fetch(`http://54.95.193.11:4001/delete-users/${user_id}`,{
            method: "POST",
            credentials: 'include',
        });
        const data = await response.json();

        if(data.status){
            getAdmins();
        }
    }

    const PostData = async(e) => {
        e.preventDefault();

        const {name, email, password} = admin;

        const res = await fetch("http://54.95.193.11:4001/admin-users",{
            method: "POST",
            credentials: 'include',
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({name, email, password})
        });

        const response = await res.json();

        if(response.status){
            new Swal(
                "Done!",
                "New Admin Created!",
                "success",
            );
        }else{
            new Swal(
                "Oooops!",
                response.msg,
                "error",
            );
        }

        // var myModal = new bootstrap.Modal(document.getElementById('addNewIcon'), {
        //             keyboard: false
        //         });

        //         myModal.hide();

        $('#addNewIcon').removeClass('show').addClass('hide');
        $('.modal-backdrop').removeClass('show').addClass('hide');

        getAdmins();
    }

    const EditData = async(e) => {
        e.preventDefault();

        const name = $("#name").val();
        const email = $("#email").val();
        const password = $("#password").val();
        const user_id = $("#userid").val();

        const res = await fetch("http://54.95.193.11:4001/update-users",{
            method: "POST",
            credentials: 'include',
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({name, email, password, user_id})
        });

        const response = await res.json();

        if(response.status){
            new Swal(
                "Done!",
                "User Updated!",
                "success",
            );
        }else{
            new Swal(
                "Oooops!",
                response.msg,
                "error",
            );
        }

        // var myModal = new bootstrap.Modal(document.getElementById('addNewIcon'), {
        //             keyboard: false
        //         });

        //         myModal.hide();

        $('#EditIcon').removeClass('show').addClass('hide');
        $('.modal-backdrop').removeClass('show').addClass('hide');

        getAdmins();
    }

    useEffect(()=>{
        getAdmins();
    },[])

    return(
            <div>
            <div className='row mb-4'>
                <div className='col-2'>
                    <a className='btn btn--primary' data-bs-toggle="modal" data-bs-target="#addNewIcon">Add new Admin</a>
                </div>
                <div className='col-3'>
                    <input placeholder='Search User' className='form-control' id='searchuser' value={search} onChange={searchAdmin}/>
                </div>
            </div>

            <div className='row'>
            <div className='col-lg-12'>
            <div className='card b-radius--10'>
            <div className='card-body p-0'>
                <div className="table-responsive--md  table-responsive">
                <table className="table table--light style--two">
                    <thead>
                        <tr>
                            <th>Serial number</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            {
                            adminsList.map((val,i)=>{
                                return(
                                    <tr key={i}>
                                        <td>{i+1}</td>
                                        <td>{val.name}</td>
                                        <td>{val.email}</td>
                                        <td>{val.addedon.substr(0,10)}</td>
                                        <td className='text-right'>
                                            <a onClick={()=>{EditUser(val.user_id)}} className='icon-btn btn btn--primary mx-1'>Edit</a>
                                            <a onClick={()=>{deleteUser(val.user_id)}} className='icon-btn btn btn--danger'>Delete</a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                </table>
                </div>
            </div>
            </div>
            </div>
            </div>

            <div className="modal fade" id="addNewIcon" tabIndex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Add New Icon</h5>
                                <button type="button" className="close btn btn-danger" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body">
                            <form method='post'>
                                <div className='form-group my-3'>
                                    <input type="text" className='form-control' name='name' required value={admin.name} onChange={handleInput} placeholder='Name'/>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="email" className='form-control' name='email' required value={admin.email} onChange={handleInput} placeholder='Email'/>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="password" className='form-control' name='password' required value={admin.password} onChange={handleInput} placeholder='Password'/>
                                </div>
                                <div className='form-group my-3'>
                                    <a type="submit" onClick={PostData} className='form-control btn btn-primary'>Submit</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="modal fade" id="EditIcon" tabIndex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Edit Icon</h5>
                                <button type="button" className="close btn btn-danger" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body">
                            <form method='post'>
                                <div className='form-group my-3'>
                                    <input type="text" className='form-control' name='editname' id='name' required placeholder='Name'/>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="email" className='form-control' name='editemail' id='email' required placeholder='Email'/>
                                </div>
                                <div className='form-group my-3'>
                                    <input type="password" className='form-control' name='editpassword' id='password' required placeholder='Password'/>
                                </div>
                                    <input type="text" name='userid' id='userid' hidden/>
                                <div className='form-group my-3'>
                                    <a type="submit" onClick={EditData} className='form-control btn btn-primary'>Submit</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ManageAdmin;