import React, {useState} from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
// import $ from "jquery";
// import bootstrap from 'bootstrap/dist/js/bootstrap';

const Login = () => {

    const [user, setUser] = useState({
        email: "",
        password: ""
    });

    const Navigate = useNavigate();

    let name, value ;

    const handleInput = (e) => {
        name = e.target.name;
        value = e.target.value;

        setUser({...user, [name]:value});
    }

    const postData = async(e) => {
        e.preventDefault();

        const {email, password} = user;

        const res = await fetch("http://54.95.193.11:4001/login",{
            method: "POST",
            credentials: 'include',
            headers: {
                "Content-Type" : "application/json",
            },
            body: JSON.stringify({email, password})
        });

        const response = await res.json();

        if(response.status){
            new Swal(
                "Done!",
                response.msg,
                "success",
            ).then((check) => {
                if(check){
                    Navigate("/manageicons");
                }
            });
        }else{
            new Swal(
                "Oooops!",
                response.msg,
                "error",
            );
        }
    }

    return(
        <>
            <section className="vh-100 bg-login">
                <div className="container py-5 h-100">
                    <div className="row d-flex align-items-center justify-content-center h-100">
                    <div className="col-md-6 col-lg-6 col-xl-6">
                        <img src="/login.png" className='login-ilus'></img>
                    </div>
                    <div className="col-md-6 col-lg-6 col-xl-6">
                        <div className='login-form-wrap'>
                            <div className='login-log text-center'>
                            <img src="/logo.png" className="img-fluid" alt="figma plugin" />
                            <div>
                                <span>Huge icon</span>
                            </div>
                        </div>
                            <form method='POST'>
                                <div className="form-outline mb-4">
                                    <input type="text" id="form1Example13" name='email' required value={user.email} onChange={handleInput} className="form-control form-control-lg" />
                                    <label className="form-label">Username</label>
                                </div>

                                <div className="form-outline mb-4">
                                    <input type="password" id="form1Example23" name='password' required value={user.password} onChange={handleInput} className="form-control form-control-lg" />
                                    <label className="form-label">Password</label>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <div className="form-check">
                                    <input className="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                                    <label className="form-check-label"> Remember me </label>
                                    </div>
                                    <a href="#!" className='a-grey'>Forgot password?</a>
                                </div>

                                    <div className='form-group mg-tpo'>
                                    <button type="submit" onClick={postData} className="btn btn-heme btn-lg btn-block">Sign in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Login;