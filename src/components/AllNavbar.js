import React from "react";
import { Outlet } from "react-router-dom";
// import Navbar from "./Navbar";
import Sidebar from "./Sidebar";

const AllNavbar = () => {
    return(
        <>
            <Sidebar/>
            <Outlet />
        </>
    )
}

export default AllNavbar;